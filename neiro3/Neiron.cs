﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace neiro3
{
    class Neiron
    {
		static Random rnd = new Random();
		public const int sizeOfVector = neironInArrayWidth* neironInArrayHeight;
        private double[,] Wi ;
		public const int neironsCount = 33;
		private double y;
		double [] sum = new double[neironsCount];
		public const int neironInArrayWidth = 10;
		public const int neironInArrayHeight = 10;
		
		private double error;

        public double GetRandomNumber(double minimum, double maximum)
		{
			return rnd.NextDouble() * (maximum - minimum) + minimum;
		}

		public Neiron(){ }

		public void SetWiForNeiron(int numNeiron)
		{
			Wi = new double[ numNeiron, sizeOfVector+1];
			double wo = GetRandomNumber(0, 1);
			for (int i = 0; i < numNeiron; i++)
			{
				for (int j = 0; j < sizeOfVector; j++)
				{
					Wi[i, j] = new double();
					Wi[i, j] = GetRandomNumber(0, 0);
				}

				Wi[i, sizeOfVector] = wo;
			}
		}
		

    public double OutputSignal(int numNeiron, int[] xi)
		{
			
				sum[numNeiron] = 0;
				for (int j = 0; j <= sizeOfVector; j++)
				{
					sum[numNeiron] += xi[j] * Wi[numNeiron, j];
					
				}

			y = 1 / (1 + Math.Pow(Math.E, -sum[numNeiron]));
			return y;
		}

		public double ErrorForNeiron( int[] di, double[] y)
		{
			error = 0;
			for (int i = 0; i < di.Length; i++)
			{
				error += Math.Pow((di[i] - y[i]),2);
			}
				
			return error/2;
		}

		public void ChangeWi(int numNeiron, double y, int d, int[]xi)
		{
			for (int i = 0; i <= sizeOfVector; i++)
			{
				Wi[numNeiron, i] = Wi[numNeiron, i] + (0.6*(y*(1-y)*(d-y)) * xi[i]);
			}
		}

    }
}
